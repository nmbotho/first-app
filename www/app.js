$(document).ready(() => {
    renderList();
    var $noteInput = $('#noteInput');
    var $noteList = $('#noteList');

    $('#addNote').click(() => {
        var noteString = $noteInput.val();
        if (noteString.length > 0) {
            var noteType = $('input[name=noteType]:checked').val();
            var note = "";
    
            if (noteType === 'meal') {
                note = 'I had '+ noteString + ' for meal.';
            } else if (noteType === 'shopping') {
                note = 'I bought ' + noteString;
            } else {
                note = 'Unknown note Type: ' + noteType;
            }
    
            addNote(note);
            $noteList.html('');
            $noteInput.val('');
            renderList();
        }
    });
    
    $('#reset').click(() => {
        $noteList.html('');
        localStorage.removeItem('notes');
    }); 
})

function addNote(note) {
    if (note) {
        var notes = JSON.parse(localStorage.getItem('notes'));
        if (!notes)
            notes = [];

        notes.push(note);
        localStorage.setItem('notes', JSON.stringify(notes));
    }
}

function renderList() {
    var notes = JSON.parse(localStorage.getItem('notes'));
    if (notes) 
        notes.forEach(item => {
            $('#noteList').append('<li>' + item + '</li>');
            $('#noteList').listview('refresh');
        });
}